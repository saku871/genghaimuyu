/**
 * 用户管理模块
 * create by 尤涛 2020-07-20
 * qq 472045067
 */
'use strict';

const NodeRSA = require('node-rsa');
const jwt = require('jsonwebtoken');

const {
	masterSmsCode,
	openExamine
} = require('config')
const uniID = require('uni-id');
const wxAuth = require('wx-auth');


const db = uniCloud.database();
const dbCmd = db.command;
const userDb = db.collection('mix-uni-id-users');

/**
 * 手机验证码核验
 * @param {String} code 验证码
 * @param {String} mobile 手机号码
 * @return {Boolean}
 * create by 尤涛 2020-07-08
 * qq 472045067
 */
const checkSmsCode = async (code, mobile) => {
	if(masterSmsCode.includes(code)){
		return true;
	}
	const res = await uniCloud.database()
		.collection('mix-sms-code')
		.where({
			mobile
		})
		.limit(1)
		.get();
	if(res.data.length === 1 && res.data[0].code === code && res.data[0].expires_time > + new Date()){
		return true;
	}
	return false;
}

const modal = {
	
	
	/**
	 * 用户提现
	 * @param {Object} request
	 * @param {String} request.uid
	 * @param {String} request.money
	 */
	async userWithDraw(request, ext){
		const {money,pay_password} = request;
		const transaction = await db.startTransaction();
		const uid = ext.uid 
		
		const userData = await db.collection('mix-uni-id-users').doc(uid).get();
		console.log('pay_password', uniID.encryptPwd(pay_password).passwordHash)
		console.log('pay_password',userData.data[0].pay_password)
		//验证支付密码
		if(!pay_password || userData.data[0].pay_password !== uniID.encryptPwd(pay_password).passwordHash){
			return {
				status: 0,
				msg: '支付密码错误',
				pay_password
			}
		}
		
		
		const res = await transaction.collection('mix-uni-id-users')
			.doc(uid)
			.update({
				money: dbCmd.inc(-money)
			})
		if(res.updated !== 1){
			await transaction.rollback()
			return {
				status: 0,
				msg: '余额修改失败'
			}
		}
		// const userData = await db.collection('mix-uni-id-users').doc(uid).get();
		//记录流水
		const logRes = await transaction.collection('mix-money-log').add({
			uid,
			title: '用户提现',
			type: 'withdraw',
			add_time: + new Date,
			money: -money,
			username: userData.data[0].username,
			pay_type: 'withdraw',
			
		})
		if(logRes.id){
			await transaction.commit()
			return {
				status: 1,
				msg: '提现成功'
			}
		}else{
			await transaction.rollback()
			return {
				status: 0,
				msg: '资金记录添加失败'
			}
		}
	},
	
	
	
	
	
	
	
	
	/**
	 * 取得用户信息
	 * @param {Object} request
	 * @param {String} request.username 手机号码
	 * @param {String} request.code 手机验证码
	 */
	async getUserInfo(request, ext){ 
		    //const userData = await db.collection('mix-uni-id-users').doc(uid).get();
			const userData = await userDb.where({_id:ext.uid}).limit(1).get();
			console.log('userData' , userData)
			return userData.data[0]
	},
	
	
	/**
	 * 手机号登录
	 * @param {Object} request
	 * @param {String} request.username 手机号码
	 * @param {String} request.code 手机验证码
	 */
	async login(request, ext){
		const {username, code} = request;
		//手机验证码核验
		let res = await checkSmsCode(code, username);
		if(!res){
			return {
				status: 0,
				msg: '验证码错误'
			}
		}
		const userData = await userDb.where({username}).limit(1).get();
		if(userData.data.length === 0){
			//新用户注册
			res = await uniID.register({
				username,
				password: '999999', //不提供密码登录功能，直接默认
			})
		}else{
			//老用户登录
			res = await uniID.login({username,password: '999999'});
		}
		return res.code === 0 ? {
			status: 1,
			data: res
		}: {
			status: 0,
			msg: res.msg
		}
	},
	/**
	 * 苹果登录
	 * @param {Object} request
	 * @param {String} request.authorizationCode 
	 * @param {String} request.identityToken 
	 */
	async loginByApple(request, ext){
		console.log('--------------11111');
		console.log(request);
		console.log('--------------222');
		const {
			authorizationCode,
			identityToken
		} = request;
		//获取JWK
		const res = await uniCloud.httpclient.request('https://appleid.apple.com/auth/keys', {
			method: 'GET',
			data: {},
			timeout: 10000,
			dataType: 'json'
		})
		console.log(res);
		console.log('--------------333');
		//解码
		const key = res.data.keys[0];
		const pubKey = new NodeRSA();
		pubKey.importKey({ n: Buffer.from(key.n, 'base64'), e: Buffer.from(key.e, 'base64') }, 'components-public');
		const data = {
			publicKey: pubKey.exportKey(['public']), 
			alg: key.alg,
		}
		console.log(data);
		console.log('--------------444');
		//jsonwebtoken 解码获取用户信息
		let payload = jwt.decode(identityToken, data.publicKey, data.alg)
		console.log(payload);
		console.log('--------------555');
		const verifyData = await new Promise(resolve=>{
			jwt.verify(identityToken, data.publicKey, (error,decoded)=> {
				resolve({
					decoded,
					error
				})
			});
		}) 
		//验证失败
		if(verifyData.error && verifyData.error.message){
			return {
				status: 0,
				msg: '登录验证失败 ' + verifyData.error.message
			}
		}
		console.log(verifyData);
		console.log('----------------666');
		const userData = await userDb.where({
			apple_id: verifyData.decoded.sub
		}).limit(1).get();
		
		if(userData.data.length === 0){
			//新用户注册
			return {
				status: 1,
				hasBindMobile: false,
				data: {
					apple_id: verifyData.decoded.sub
				}
			}
		}
		const loginRes = await uniID.login({
			username: userData.data[0].username,
			password: '999999',
		});
		return loginRes.code === 0 ? {
			status: 1,
			hasBindMobile: true,
			data: loginRes
		}: {
			status: 0,
			msg: loginRes.msg || '登录失败，请稍候再试'
		}
	},	
	/**
	 * 苹果 注册绑定手机号
	 * @param {Object} request
	 * @param {String} request.username 手机号码
	 * @param {String} request.code 手机验证码
	 * @param {String} request.apple_id 头像
	 */
	async appleRegister(request, ext){
		const {
			username,
			code,
			apple_id
		} = request;
		
		//手机验证码核验
		let res = await checkSmsCode(code, username);
		if(!res){
			return {
				status: 0,
				msg: '验证码错误'
			}
		}
		const oldUser = await userDb.where({
			username
		}).limit(1).get();
		if(oldUser.data.length === 0){
			//新用户注册
			res = await userDb.add({
				username,
				password: uniID.encryptPwd('999999'), //不提供密码登录功能，直接默认
				apple_id,
				register_date: + new Date(),
				register_ip: ext.context.CLIENTIP
			});
			if(!res.id){
				return {
					status: 0,
					msg: '注册用户失败'
				}
			}
		}else{
			//老用户绑定苹果id
			res = await userDb.doc(oldUser.data[0]._id).update({
				apple_id
			});
			if(res.updated !== 1){
				return {
					status: 0,
					msg: '微信绑定失败',
					res,
					id: oldUser.data[0]._id,
					apple_id
				}
			}
		}
		res = await uniID.login({username,password: '999999'});
		return res.code === 0 ? {
			status: 1,
			data: res
		}: {
			status: 0,
			msg: res.msg
		}
	},
	/**
	 * 微信登录
	 * @param {Object} request
	 * @param {String} code
	 */
	async loginByWeixin(request, ext){
		let wxUserInfo = {};
		
		if(ext.context.PLATFORM === 'app-plus'){
			//app登录
			wxUserInfo = request.userInfo;
		}else{
			//小程序登录
			const {code, encryptedData, iv} = request;
			//获取session_key
			wxUserInfo = await wxAuth.mpWxGetSessionKey({
				code
			})
			if(wxUserInfo.status === 0){
				return wxUserInfo;
			}
			wxUserInfo.avatarUrl = request.avatarUrl;
			wxUserInfo.nickName = request.nickName;
			wxUserInfo.gender = request.gender;
		}
		const {
			avatarUrl: avatar,
			nickName: nickname,
			gender,
			openId,
			unionId
		} = wxUserInfo;
		let userData;
		if(unionId){
			userData =  await userDb.where({
				wx_unionid: unionId
			}).limit(1).get();
		}else{
			userData =  await userDb.where({
				wx_openid: {
					'mp-weixin': openId
				}
			}).limit(1).get();
		}
		if(userData.data.length === 0){
			//新用户注册
			return {
				status: 1,
				hasBindMobile: false,
				data: {
					avatar,
					nickname,
					gender,
					openId,
					unionId
				}
			}
		}
		const res = await uniID.login({
			username: userData.data[0].username,
			password: '999999',
		});
		return res.code === 0 ? {
			status: 1,
			hasBindMobile: true,
			data: res
		}: {
			status: 0,
			msg: res.msg || '登录失败，请稍候再试'
		}
	},
	/**
	 * 微信注册绑定手机号
	 * @param {Object} request
	 * @param {String} request.username 手机号码
	 * @param {String} request.code 手机验证码
	 * @param {String} request.avatar 头像
	 * @param {String} request.nickname 昵称
	 * @param {String} request.gender 性别
	 * @param {String} request.openId 
	 * @param {String} request.unionId 绑定开放平台且多端必填
	 */
	async wxRegister(request, ext){
		const {
			username,
			code,
			avatar,
			nickname,
			gender,
			openId,
			unionId
		} = request;
		
		//手机验证码核验
		let res = await checkSmsCode(code, username);
		if(!res){
			return {
				status: 0,
				msg: '验证码错误'
			}
		}
		const wx_openid = {};
		if(ext.context.PLATFORM === 'mp-weixin'){
			wx_openid['mp-weixin'] = openId;
		}else if(ext.context.PLATFORM === 'app-plus'){
			wx_openid['app-plus'] = openId;
		}
		const data = {
			wx_unionid: unionId
		}
		if(JSON.stringify(wx_openid) !== '{}'){
			data.wx_openid = wx_openid;
		}
		const oldUser = await userDb.where({
			username
		}).limit(1).get();
		
		if(oldUser.data.length === 0){
			//新用户注册
			res = await userDb.add({
				username,
				password: uniID.encryptPwd('999999').passwordHash, //不提供密码登录功能，直接默认
				avatar,
				nickname,
				gender,
				register_date: + new Date(),
				register_ip: ext.context.CLIENTIP,
				...data
			});
			if(!res.id){
				return {
					status: 0,
					msg: '注册用户失败'
				}
			}
		}else{
			//老用户绑定微信
			res = await userDb.doc(oldUser.data[0]._id).update(data);
			if(res.updated !== 1){
				return {
					status: 0,
					msg: '微信绑定失败',
					res,
					id: oldUser.data[0]._id,
					data
				}
			}
		}
		res = await uniID.login({username,password: '999999'});
		return res.code === 0 ? {
			status: 1,
			data: res
		}: {
			status: 0,
			msg: res.msg
		}
	},
	/**
	 * 退出登录
	 */
	async logout(request, ext){
		const res = await uniID.logout(ext.event.uniIdToken);
		return res.code === 0 ? {
			status: 1
		}: {
			status: 0,
			msg: res.msg
		}
	},
	/**
	 * 获取用户信息
	 */
	async get(request, ext){
		if(!ext.uid){
			return {
				status: 0,
				msg: '用户未登录',
				openExamine
			}
		}
		const res = await userDb
		
		.doc(ext.uid)
		
		.get()
		
		return res.data.length === 1 ? {
			status: 1,
			data: res.data[0],
			openExamine
		}: {
			status: 0,
			msg: '用户不存在',
			openExamine
		}
	},
	/**
	 * 更新用户信息
	 * @param {Object} request
	 * @param {String} request.avatar 头像
	 * @param {String} request.nickname 昵称
	 * @param {Number} request.gender 1男 2女 0保密
	 * @param {Boolean} request.anonymous 是否隐藏个人信息
	 * @param {Boolean} request.receive_push 是否接收推送通知
	 */
	async update(request, ext){
		//允许更新字段
		const attrs = ['avatar', 'nickname', 'gender', 'anonymous', 'receive_push','phone', 'identity', 'describe', 'images',];
		const data = {};
		for(let key in request){
			if(attrs.includes(key)){
				data[key] = request[key];
			}
		}
		const res= await userDb
		.doc(ext.uid)
		
		
		.update(data);
		
		// res.data.forEach((item, index)=> {
			
		// 	res.data[index].identity = item.ReleaseIdentity[0].identity ? item.ReleaseIdentity[0].identity : {};
			
		// })
		return res.updated === 1 ? {
			status: 1,
			msg: '信息更新成功'
		}: {
			status: 0,
			msg: '信息修改失败'
		}
	},
	/**
	 * 设置支付密码
	 * @param {Object} request
	 * @param {String} request.username 用户名(手机号)
	 * @param {String} request.pay_password 支付密码
	 * @param {String} request.code 手机验证码
	 */
	async setPayPasswod(request, ext){
		const {username, pay_password, code} = request;
		//手机验证码核验
		const checkCodeRes = await checkSmsCode(code, username);
		if(!checkCodeRes){
			return {
				status: 0,
				msg: '验证码错误'
			};
		}
		const res= await userDb.doc(ext.uid).update({
			pay_password: uniID.encryptPwd(pay_password).passwordHash
		});
		return res.updated === 1 ? {
			status: 1,
			msg: '支付密码已重置'
		}: {
			status: 0,
			msg: '设置失败'
		}
	}
}

module.exports = modal;