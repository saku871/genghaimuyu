/**
 * 收货地址管理模块
 * create by 尤涛 2020-07-05
 * qq 472045067
 */
'use strict';

const db = uniCloud.database();
const dbCmd = db.command;
const loanDb = db.collection('mix-withdraw');
const $ = db.command.aggregate;

const modal = {
	
	
	async getList(request, ext){
		const {offset, limit, mobile, name} = request;
		const map = {};
		if(mobile) map.mobile = new RegExp(mobile)
		if(name) map.name = new RegExp(name)
		
		const res = await loanDb
		
		    .aggregate()
			
			.match(map)
			.sort({
				...request.orderBy,
				register_date: -1
			})
			.skip(offset)
			.limit(limit)
			
			.lookup({
				from: "mix-release-category",
				localField: "category",
				foreignField: "_id",
				as: 'category'
			}).lookup({
				from: "mix-release-category",
				localField: "fresh",
				foreignField: "_id",
				as: 'fresh'
			}).lookup({
				from: "mix-release-category",
				localField: "specifications",
				foreignField: "_id",
				as: 'specifications'
			})
			
			.end();
			if(res.data.length > 0){
			
			}	
		const countData = await loanDb.count();
		res.affectedDocs = countData.total;
		return res;
	},

	
	/**
	 * 新增地址
	 * @param {Object} request
	 */
	async add(request, ext){
		const data = {
			...request,
			
			status:0,
			uid: ext.uid,
			add_time: + new Date()
		}
		const userData = await db.collection('mix-uni-id-users').doc(ext.uid).get();
		data.username = userData.data[0].username,
		data.phone = userData.data[0].phone,
		data.nickname = userData.data[0].nickname,
		
		console.log('1111111111111')
		const res= await loanDb.add(data); console.log('22222222222')
		if(data.is_default){
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}
		
		return res.id ? {
			status: 1,
			uid:ext.uid,
			msg: '提现成功'
		}: {
			status: 0,
			msg: '提现成功，请稍候再试'
		}
	},
	/**
	 * 删除
	 * @param {Object} request
	 * @param {String} request.id
	 */
	async remove(request){
		const res= await loanDb.doc(request.id).remove();
		return res.deleted === 1 ? {
			status: 1,
			msg: '删除成功'
		}: {
			status: 0,
			msg: '删除失败，请稍候再试'
		}
	},
	/**
	 * 修改
	 * @param {Object} request
	 */
	async update(request, ext){
		const id = request._id;
		delete request._id;
		const data = {
			...request,
			update_time: + new Date()
		}
		data.uid = ext.uid; 
		
		if(data.is_default){
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}
		const res= await loanDb.doc(id).set(data);
		return res.updated === 1 ? {
			status: 1,
			msg: '修改成功'
		}: {
			status: 0,
			msg: res.message || '服务器内部错误'
		}
	},
	/**
	 * 获取列表
	 */
	async get(request, ext){
		console.log('request', request)
		const res= await loanDb
			.where({
				_id: request._id
			})
			.get();
			
		return res;
	},
	
	
	
	
	
	
	
	
}

module.exports = modal;