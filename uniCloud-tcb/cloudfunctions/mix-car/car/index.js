/**
 * 收货地址管理模块
 * create by 尤涛 2020-07-05
 * qq 472045067
 */
'use strict';

const db = uniCloud.database();
const dbCmd = db.command;
const loanDb = db.collection('mix-car');
const $ = db.command.aggregate;

const modal = {
	async getDetail(request, ext){
		const res = await loanDb.aggregate()
			.match({
				_id: request.id,
				// is_del: 0
			})
			.limit(1)
			.end();
		//限制产品处理
		if(res.data.length === 0){
			return {
				status: 0,
				msg: '没有找到相关产品'
			}
		}
		
		console.log('res.data[0]' ,  res.data[0] ) 
		await loanDb.where({_id: request.id}).update({look_num: dbCmd.inc(1)})
		
		return {
			status: 1,
			data: res.data[0]
		}
	},
	
	async getList(request, ext){
		const {offset, limit, mobile, name,chauid} = request;
		const map = {};
		if(mobile) map.mobile = new RegExp(mobile)
		if(name) map.name = new RegExp(name)
		console.log('chauid' , chauid)
		if(chauid) map.uid = ext.uid
		const res = await loanDb
		
		    .aggregate()
			
			.match(map)
			.sort({
				...request.orderBy,
				register_date: -1
			})
			.skip(offset)
			.limit(limit)
			
			.lookup({
				from: "mix-release-category",
				localField: "category",
				foreignField: "_id",
				as: 'category'
			}).lookup({
				from: "mix-release-category",
				localField: "fresh",
				foreignField: "_id",
				as: 'fresh'
			}).lookup({
				from: "mix-release-category",
				localField: "specifications",
				foreignField: "_id",
				as: 'specifications'
			})
			
			.end();
			if(res.data.length > 0){
			
			}	
		const countData = await loanDb.count();
		res.affectedDocs = countData.total;
		if(chauid) res.ismyself = 1
		else  res.ismyself = 0
		return res;
	},
	
	
	/**
	 * 分类页 获取产品列表
	 * @param {Object} request
	 * @param {Number} request.offset
	 * @param {Number} request.limit
	 * @param {sort_type} request.sort_type 排序类型 1默认 2销量优先 3价格升序 4价格降序 5好评优先
	 * @param {cate_id} request.first_cate_id 产品一级分类id
	 * @param {cate_id} request.cate_id 产品二级分类id
	 * @param {keyword} request.keyword 搜索关键字
	 */
	
	
	// async getLogList(request, ext){
	// 	const {offset, limit, username, payType, type} = request;
	// 	const map = {}
		
	// 	if(username) map.username = new RegExp(username);
	// 	if(payType) map.pay_type = new RegExp(payType);
	// 	if(type) map.type = type;
		
	// 	const res = await loanDb.collection('mix-money-log').aggregate()
	// 		.match(map)
	// 		.sort({
	// 			add_time: -1
	// 		})
	// 		.skip(offset)
	// 		.limit(limit)
	// 		.lookup({
	// 			from: "mix-uni-id-users",
	// 			localField: "uid",
	// 			foreignField: "_id",
	// 			as: 'user'
	// 		})
	// 		.end();
	// 	const countData = await db.collection('mix-money-log').where(map).count();
	// 	res.affectedDocs = countData.total;
	// 	res.data.forEach(item=> {
	// 		item.user = item.user.length > 0 ? item.user[0] : {};
	// 	})
	// 	return res;
	// },
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	/**
	 * 新增地址
	 * @param {Object} request
	 */
	async add(request, ext){
		const data = {
			...request,
			uid: ext.uid,
			add_time: + new Date()
		}
		console.log('1111111111111')
		const res= await loanDb.add(data); console.log('22222222222')
		if(data.is_default){
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}
		
		return res.id ? {
			status: 1,
			uid:ext.uid,
			msg: '发布成功'
		}: {
			status: 0,
			msg: '发布失败，请稍候再试'
		}
	},
	/**
	 * 删除
	 * @param {Object} request
	 * @param {String} request.id
	 */
	async remove(request){
		const res= await loanDb.doc(request.id).remove();
		return res.deleted === 1 ? {
			status: 1,
			msg: '删除成功'
		}: {
			status: 0,
			msg: '删除失败，请稍候再试'
		}
	},
	/**
	 * 修改
	 * @param {Object} request
	 */
	async update(request, ext){
		const id = request._id;
		delete request._id;
		const data = {
			...request,
			update_time: + new Date()
		}
		data.uid = ext.uid; 
		
		if(data.is_default){
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}
		const res= await loanDb.doc(id).set(data);
		return res.updated === 1 ? {
			status: 1,
			msg: '修改成功'
		}: {
			status: 0,
			msg: res.message || '服务器内部错误'
		}
	},
	/**
	 * 获取列表
	 */
	async get(request, ext){
		console.log('request', request)
		const res= await loanDb
			.where({
				_id: request._id
			})
			.get();
			
		return res;
	},
	
	
	
	
	
	
	
	
}

module.exports = modal;