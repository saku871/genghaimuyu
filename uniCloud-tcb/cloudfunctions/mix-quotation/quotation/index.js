/**
 * 收货地址管理模块
 * create by 尤涛 2020-07-05
 * qq 472045067
 */
'use strict';

const db = uniCloud.database();
const dbCmd = db.command;
const loanDb = db.collection('mix-quotation');

const modal = {



	async getList(request, ext) {

		console.log('getList(request', JSON.stringify(request))

		const {
			offset,
			limit,
			cate_id,
			name,
			curList
		} = request;
		const map = {
			status: 1,
		}
		if (cate_id) {
			map.category = cate_id
		}
		if (name) {
			map.name = name
		}
		// if(curList) {
		// 	map.curList = curList
		// }
		console.log('getListgetList', JSON.stringify(map))

		const $ = db.command.aggregate
		const res = await loanDb.aggregate()
			.group({
				_id: {
					name: '$name',

				},
				name: $.last('$name'),
				price: $.last('$price'),
				add_time: $.last('$add_time'),
				pre_price: $.last('$pre_price')
			}).end();
		// .match(map)
		// .skip(offset)

		//.limit(1)
		// .lookup({
		// 	from: "mix-release-category",
		// 	localField: "specifications",
		// 	foreignField: "_id",
		// 	as: 'ReleaseSpecifications',

		// })
		// .lookup({
		// 	from: "mix-release-category",
		// 	localField: "fresh",
		// 	foreignField: "_id",
		// 	as: 'ReleaseFresh'
		// })
		// .lookup({
		// 	from: "mix-release-category",
		// 	localField: "category",
		// 	foreignField: "_id",
		// 	as: 'ReleaseCategory'
		// })


		// const countData = await loanDb.count();
		// res.affectedDocs = countData.total;
		// res.data.forEach((item, index)=> {
		// 	 console.log('item + '+JSON.stringify(item))
		// 	//res.data[index].companyname = item.renzheng[0].companyname ? item.renzheng[0].companyname : {};
		// })	  


		const countData = await loanDb.where(map).count();
		res.affectedDocs = countData.total;
		res.data.forEach((item, index) => {
			console.log(item)
			let bili = 0
			if (item.pre_price > item.price) {
				res.data[index].opreation_image = "../../static/down.png";
				res.data[index].class = "green";
			} else if (item.pre_price < item.price) {
				res.data[index].opreation_image = "../../static/top.png";
				res.data[index].class = "red";
			} else {
				res.data[index].opreation_image = "";
			}
			bili = (Number(item.price) - Number(item.pre_price)) / Number(item.pre_price);
			bili = bili.toFixed(2);
			res.data[index].bili = Math.abs(bili);
			// res.data[index].specifications = item.ReleaseSpecifications[0].specifications ? item.ReleaseSpecifications[0].specifications : {};
			// res.data[index].category = item.ReleaseCategory[0].category ? item.ReleaseCategory[0].category : {};
			// res.data[index].fresh = item.ReleaseFresh[0].fresh ? item.ReleaseFresh[0].fresh : {};
		})

		return res;

	},
	async getquoList(request, ext){
		 
		console.log('getList(request', JSON.stringify(request)) 
		 
		const {offset, limit, cate_id,name,curList } = request;
		const map = {
		 	status: 1, 
		}
		if(cate_id) {
			map.category = cate_id
		}
		if(name) {
			map.name = name
		}
		if(curList) {
			map.curList = curList
		}
		console.log('getListgetList' , JSON.stringify(map))
		const res= await loanDb.aggregate()
			.match(map)
			.skip(offset)
			.limit(limit)
			.lookup({
				from: "mix-release-category",
				localField: "specifications",
				foreignField: "_id",
				as: 'ReleaseSpecifications',
				
			})
			.lookup({
				from: "mix-release-category",
				localField: "fresh",
				foreignField: "_id",
				as: 'ReleaseFresh'
			})
			.lookup({
				from: "mix-release-category",
				localField: "category",
				foreignField: "_id",
				as: 'ReleaseCategory'
			})
			.end();
		       
			// const countData = await loanDb.count();
			// res.affectedDocs = countData.total;
			// res.data.forEach((item, index)=> {
			// 	 console.log('item + '+JSON.stringify(item))
			// 	//res.data[index].companyname = item.renzheng[0].companyname ? item.renzheng[0].companyname : {};
			// })	  
		
			
			const countData = await loanDb.where(map).count();
			res.affectedDocs = countData.total;
			res.data.forEach((item, index)=> {
				
				// res.data[index].specifications = item.ReleaseSpecifications[0].specifications ? item.ReleaseSpecifications[0].specifications : {};
				// res.data[index].category = item.ReleaseCategory[0].category ? item.ReleaseCategory[0].category : {};
				// res.data[index].fresh = item.ReleaseFresh[0].fresh ? item.ReleaseFresh[0].fresh : {};
			})
			
		    return res;
			
	},


	/**
	 * 新增地址
	 * @param {Object} request
	 */
	async add(request, ext) {
		const data = {
			...request,
			uid: ext.uid,
			add_time: +new Date()
		}
		console.log('1111111111111')
		const res = await loanDb.add(data);
		console.log('22222222222')
		if (data.is_default) {
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}

		return res.id ? {
			status: 1,
			uid: ext.uid,
			msg: '贷款申请成功'
		} : {
			status: 0,
			msg: '贷款申请失败，请稍候再试'
		}
	},
	/**
	 * 删除
	 * @param {Object} request
	 * @param {String} request.id
	 */
	async remove(request) {
		const res = await loanDb.doc(request.id).remove();
		return res.deleted === 1 ? {
			status: 1,
			msg: '贷款申请删除成功'
		} : {
			status: 0,
			msg: '贷款申请删除失败，请稍候再试'
		}
	},
	/**
	 * 修改
	 * @param {Object} request
	 */
	async update(request, ext) {
		const id = request._id;
		delete request._id;
		const data = {
			...request,
			update_time: +new Date()
		}
		if (data.is_default) {
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}
		const res = await loanDb.doc(id).set(data);
		return res.updated === 1 ? {
			status: 1,
			msg: '贷款申请修改成功'
		} : {
			status: 0,
			msg: res.message || '服务器内部错误'
		}
	},


}

module.exports = modal;
