'use strict';
const {
	wxConfigMp,
	wxConfigApp,
	aliConfigMp,
	aliConfigApp,
	paymentNotifyUrl,
} = require('config')

const uniPay = require('unipay')
const uniID = require('uni-id')
const wxAuth = require('wx-auth');

const db = uniCloud.database();
const dbCmd = db.command;

/**
 * 格式化时间戳 Y-m-d H:i:s
 * @param {String} format Y-m-d H:i:s
 * @param {Number} timestamp 时间戳   
 * @return {String}
 */
const date = (format, timeStamp) => {
	if('' + timeStamp.length <= 10){
		timeStamp = + timeStamp * 1000;
	}else{
		timeStamp = + timeStamp;
	}
	let _date = new Date(timeStamp),
		Y = _date.getFullYear(),
		m = _date.getMonth() + 1,
		d = _date.getDate(),
		H = _date.getHours(),
		i = _date.getMinutes(),
		s = _date.getSeconds();
	
	m = m < 10 ? '0' + m : m;
	d = d < 10 ? '0' + d : d;
	H = H < 10 ? '0' + H : H;
	i = i < 10 ? '0' + i : i;
	s = s < 10 ? '0' + s : s;

	return format.replace(/[YmdHis]/g, key=>{
		return {Y,m,d,H,i,s}[key];
	});
}
/**
 * 退款单号生成 20位
 * @return {String} 单号
 */
const createRandomNo = ()=>{
	let random_no = date('Ymd', +new Date());
	for (let i = 0; i < 12; i++){
		random_no += Math.floor(Math.random() * 10);
	}
	return random_no;
}
/**
 * 余额支付
 * @param {Object} param
 * @param {Object} param.user 用户
 * @param {Number} param.totalFee 支付金额 分
 * @param {String} param.pay_password 可选，余额支付需要
 */
const balancePay = async (param)=>{
	const {user, totalFee, pay_password} = param;
	//验证支付密码
	if(!pay_password || user.pay_password !== uniID.encryptPwd(pay_password).passwordHash){
		return {
			status: 0,
			msg: '支付密码错误',
			pay_password
		}
	}
	//验证余额
	if(!user.money || user.money*100 < totalFee){
		return {
			status: 0,
			msg: '账户余额不足'
		}
	}
	//扣除用户余额
	const res = await db.collection('mix-uni-id-users').doc(user._id)
		.update({
			money: dbCmd.inc(- totalFee/100)
		})
	return res.updated === 1 ? {
		status: 1
	}:{
		status: 0,
		msg: '余额扣除失败'
	}
}

/**
 * 初始化unipay
 * @param {Object} param
 * @param {String} param.provider 支付方式
 * @param {String} param.PLATFORM 调用平台
 * 
 * @param {String} param.code 可选，小程序code 小程序支付必须 
 */
const initUniPay = async param=>{
	const {provider, PLATFORM, code} = param;
	let uniPayInstance, openid;
	switch (provider + '_' + PLATFORM) {
		case 'wxpay_mp-weixin':
			uniPayInstance = uniPay.initWeixin(wxConfigMp)
			if(code){
				const codeSession = await wxAuth.mpWxGetSessionKey({
					code
				});
				openid = codeSession.openId;
			}
			break;
		case 'alipay_mp-alipay':
			uniPayInstance = uniPay.initAlipay(aliConfigMp)
			//openid = user.ali_openid
			break;
		case 'wxpay_app-plus':
			uniPayInstance = uniPay.initWeixin(wxConfigApp)
			break;
		case 'alipay_app-plus':
			uniPayInstance = uniPay.initAlipay(aliConfigApp)
			break;
		default:
			return {
				status: -1,
				msg: '参数错误'
			}
	}
	return {
		uniPayInstance,
		openid
	}
}
/**
 * unipay支付
 * @param {Object} param
 * @param {String} param.outTradeNo 订单号
 * @param {Number} param.totalFee 支付金额, 单位分
 * @param {String} param.subject 订单标题
 * @param {String} param.body 商品描述
 * @param {String} param.pay_type 支付方式 balance wxpay alipay
 * @param {String} param.notifyUrl 回调地址
 * 
 * @param {String} param.code 可选，小程序code 小程序支付必须 
 */
const payByUnipay = async (param, ext)=>{
	const {outTradeNo, totalFee, subject, body, pay_type: provider, code, notifyUrl} = param;
	const PLATFORM =  ext.context.PLATFORM;
	const {uniPayInstance, openid} = await initUniPay({
		provider,
		PLATFORM,
		code
	})
	let orderInfo;
	try {
		// 获取支付信息
		orderInfo = await uniPayInstance.getOrderInfo({
			openid, // App端支付时不需要openid，传入个undefined也没有影响
			outTradeNo,
			totalFee: parseInt(totalFee),
			subject,
			body,
			notifyUrl: `${notifyUrl}/${provider}_${PLATFORM}`
		})
	} catch (e) {
		return {
			status: -3,
			msg: typeof e.message === 'string' ? e.message : '获取支付信息失败，请稍后再试',
			err: e.message
		}
	}
	return {
		status: 1,
		data: {
			outTradeNo,
			orderInfo
		}
	}
}

const modal = {
	/**
	 * 用户余额充值
	 * @param {Object} request
	 * @param {Number} request.money 充值金额
	 * @param {String} request.pay_type 支付方式
	 * @param {String} request.code 小程序code 小程序充值必须 
	 */
	async recharge(request, ext){
		const {money, code, pay_type} = request;
		const uid = ext.uid;
		if(!uid){
			return {
				status: 0,
				msg: '获取用户信息失败'
			}
		}
		if(isNaN(money) || money <= 0){
			return {
				status: 0,
				msg: '充值金额错误'
			}
		}
		const order_number = createRandomNo();
		const data = {
			order_number,
			uid,
			money,
			price_data: {
				pay_price: money
			},
			pay_type: pay_type,
			platform: ext.context.PLATFORM,
			add_time: +new Date(),
			pay_status: 0
		}
		let res = await db.collection('mix-recharge').add(data);
		if(!res.id){
			return {
				status: 0,
				msg: '订单创建失败，请稍候再试'
			}
		}
		
		res = await payByUnipay({
			outTradeNo: order_number,
			totalFee: money * 100,
			subject: '预存款充值',
			body: '用户预存款账户充值',
			pay_type,
			code,
			notifyUrl: paymentNotifyUrl + '/recharge',
		}, ext);
		return res;
	},
	/**
	 * 支付
	 * @param {Object} param
	 * @param {String} param.pay_type 支付方式 balance余额 alipay支付宝 wxpay微信支付
	 * @param {Object} param.order 订单
	 * @param {Object} param.user 用户
	 * @param {String} param.pay_password 支付密码(余额支付时)
	 */
	async pay(param, ext){
		if(param.pay_type === 'balance'){
			return await balancePay(param);
		}
		return await payByUnipay({
			notifyUrl: paymentNotifyUrl + '/payOrder',
			...param
		}, ext);
	},
	/**
	 * 退款
	 * @param {Object} params
	 * @param {String} params.uid
	 * @param {Object} params.order 订单
	 * @param {Number} params.money 
	 * @param {Number} params.outRefundNo 商户退款单号退款单号
	 */
	async refund(params, ext){
		const {uid, order, money, outRefundNo, transaction} = params;
		//余额退款
		if(order.pay_type === 'balance'){
			const res = await transaction.collection('mix-uni-id-users')
				.doc(uid)
				.update({
					money: dbCmd.inc(money)
				})
			return res.updated === 1 ? {
				status: 1
			}:{
				status: 0,
				msg: '退款失败'
			}
		}
		//uniPay退款
		const {uniPayInstance} = await initUniPay({
			provider: order.pay_type,
			PLATFORM: ext.context.PLATFORM
		})
		try {
			const options = {
			    outTradeNo: order.order_number,
			    outRefundNo, // 支付宝可不填此项
			    totalFee: money * 100, // 订单总金额，支付宝可不填此项
			    refundFee: money * 100, // 退款总金额
			}
			const res = await uniPayInstance.refund(options)
			if(res.returnCode === 'SUCCESS' && res.resultCode === 'SUCCESS'){
				return {
					status: 1
				}
			}else{
				return {
					status: 0,
					msg: res.returnMsg || '请求退款失败'
				}
			}
		}catch(err){
			console.log(err);
			return {
				status: 0,
				msg: '退款失败，请稍候再试',
			}
		}
	},
	/**
	 * 获取用户资金记录
	 * @param {Number} request.offset
	 * @param {Number} request.limit
	 */
	async getMoneyLog(request, ext){
		const map ={
				uid: ext.uid
			}
			
		if(request.navCurrent==1) { 
			map.money = dbCmd.lt(0) 
		}
		if(request.navCurrent==0) { 
			map.money = dbCmd.gt(0) 
		}		
		console.log('request.dates', request.dates)
		
		if(request.dates) {
			
			let dates = request.dates
			 
			let startTimestamp = +new Date( dates +' 00:00:00');
			let endTimestamp = +new Date( dates +' 23:59:59');
			 console.log('startTimestamp', startTimestamp)
		  
			 map.add_time=dbCmd.gt(startTimestamp).and(dbCmd.lt(endTimestamp))
			
			console.log('endTimestamp', endTimestamp)
		
			
		}

		
		
		const res = await db.collection('mix-money-log')
			.skip(request.offset)
			.limit(request.limit)
			.where(map)
			.orderBy('add_time', 'desc')
			.get()
		return res;
	},
}
module.exports = modal;