'use strict';


const uniID = require('uni-id');
const modules = {
	yugou: require('./yugou'),
}

exports.main = async (event, context) => {
	const {
		module,
		operation,
		data,
		uniIdToken
	} = event;

	let skip_check = ["get"]
	if (!skip_check.includes(operation)) {
		const payload = await uniID.checkToken(uniIdToken);
		if (payload.code && payload.code > 0) {
			return {
				op: 'token 检查失败',
				...payload
			}
		}
	}
	//token检查

	const ext = {
		event,
		context,
		uid: payload.uid
	}

	return modules[module][operation](data, ext);
};
