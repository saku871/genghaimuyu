/**
 * 收货地址管理模块
 * create by 尤涛 2020-07-05
 * qq 472045067
 */
'use strict';

const db = uniCloud.database();
const dbCmd = db.command;
const loanDb = db.collection('mix-yugou');

const modal = {
	async getList(request){
		const {offset, limit,  category, specifications,} = request; // , cate_id, keyword, is_hot
		
		//排序
		// const orderBy = [
		// 	{field: 'add_time', type: 'desc'},   //升序
		// 	{field: 'sales', type: 'desc'},
		// 	{field: 'price', type: 'asc'},      // 降序
		// 	{field: 'price', type: 'desc'},
		// 	{field: 'rating', type: 'desc'},
		// ][(sort_type || 1) - 1]
		//筛选
		const map = {
			status: 0, 
		}
		
		if(category) map.category = category;
	    if(specifications) map.specifications = specifications;
		// if(cate_id) map.cate_id = cate_id;
		// if(keyword) map.title = new RegExp(keyword);
		// if(is_hot == 1) map.is_hot = 1;
		
		const res = await loanDb.aggregate()
			.match(map)
			.sort({
				add_time: -1
			})
			.skip(offset)
			.limit(limit)
			.lookup({
				from: "mix-renzheng",
				localField: "uid",
				foreignField: "uid",
				as: 'renzheng'
			})
			.end();
			
			const countData = await loanDb.where(map).count();
			res.affectedDocs = countData.total;
			res.data.forEach((item, index)=> {
				 console.log('item + '+JSON.stringify(item))
				res.data[index].companyname = item.renzheng[0].companyname ? item.renzheng[0].companyname : {};
			})
			console.log('data',res)
		 
		return res;
	},
	
	
	/**
	 * 新增地址
	 * @param {Object} request
	 */
	async add(request, ext){
		const data = {
			...request,
			uid: ext.uid,
			add_time: + new Date()
		}
		console.log('1111111111111')
		const res= await loanDb.add(data); console.log('22222222222')
		if(data.is_default){
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}
		
		return res.id ? {
			status: 1,
			uid:ext.uid,
			msg: '贷款申请成功'
		}: {
			status: 0,
			msg: '贷款申请失败，请稍候再试'
		}
	},
	/**
	 * 删除
	 * @param {Object} request
	 * @param {String} request.id
	 */
	async remove(request){
		const res= await loanDb.doc(request.id).remove();
		return res.deleted === 1 ? {
			status: 1,
			msg: '贷款申请删除成功'
		}: {
			status: 0,
			msg: '贷款申请删除失败，请稍候再试'
		}
	},
	/**
	 * 修改
	 * @param {Object} request
	 */
	async update(request, ext){
		const id = request._id;
		delete request._id;
		const data = {
			...request,
			update_time: + new Date()
		}
		if(data.is_default){
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}
		const res= await loanDb.doc(id).set(data);
		return res.updated === 1 ? {
			status: 1,
			msg: '贷款申请修改成功'
		}: {
			status: 0,
			msg: res.message || '服务器内部错误'
		}
	},
	/**
	 * 获取列表
	 */
	async get(request, ext){
		const res= await loanDb
			.where({
				uid: ext.uid
			})
			.orderBy('is_default', 'desc')
			.orderBy('add_time', 'desc')
			.get();
			
			res.uid = 	ext.uid ;
		    return res;
	}
}

module.exports = modal;