/**
 * 收货地址管理模块
 * create by 尤涛 2020-07-05
 * qq 472045067
 */
'use strict';

const db = uniCloud.database();
const dbCmd = db.command;
const loanDb = db.collection('mix-renzheng1');

const modal = {
	/**
	 * 新增地址
	 * @param {Object} request
	 */
	async add(request, ext){
		const data = {
			...request,
			uid: ext.uid,
			add_time: + new Date()
		}
		console.log('1111111111111')
		const res= await loanDb.add(data); console.log('22222222222')
		if(data.is_default){
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}
		
		return res.id ? {
			status: 1,
			msg: '证件信息提交成功'
		}: {
			status: 0,
			msg: '证件信息提交失败，请稍候再试'
		}
	},
	/**
	 * 删除
	 * @param {Object} request
	 * @param {String} request.id
	 */
	async remove(request){
		const res= await loanDb.doc(request.id).remove();
		return res.deleted === 1 ? {
			status: 1,
			msg: '证件信息删除成功'
		}: {
			status: 0,
			msg: '证件信息删除失败，请稍候再试'
		}
	},
	/**
	 * 修改
	 * @param {Object} request
	 */
	async update(request, ext){
		const id = request._id;
		delete request._id;
		console.log('update id' +  id )
		
		const data = {
			...request,
			update_time: + new Date()
		}
		if(data.is_default){
			await loanDb
				.where({
					uid: ext.uid,
					is_default: true
				})
				.update({
					is_default: false
				})
		}
		console.log('update id 2 ' +  id )
		const res= await loanDb.doc(id).set(data);
		return res.updated === 1 ? {
			status: 1,
			msg: '证件信息修改成功'
		}: {
			status: 0,
			msg: res.message || '服务器内部错误'
		}
	},
	/**
	 * 获取列表
	 */
	
	
	
	async get(request, ext){
		const res= await loanDb
			.where({
				uid: ext.uid
			})
			.orderBy('add_time', 'desc')
			.limit(1).get()
			
			return res ; 
		
	}
}

module.exports = modal;